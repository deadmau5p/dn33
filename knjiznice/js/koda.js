/* global L*/
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var map;


var bolnice = [];
var layers = [];


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";
  console.log("here");
  var ime;
  var priimek;
  var datumRojstva;
  
  // TODO: Potrebno implementirati
  if(stPacienta == 1){
    ime = "Alphonse";
    priimek = "Elric";
    datumRojstva = "1900-04-01";
  }
  else if(stPacienta == 2){
    ime = "Tsumugi";
    priimek = "Kotobuki";
    datumRojstva = "1991-07-02";
  }
  else if(stPacienta == 3){
    ime = "Giorno";
    priimek = "Giovanna";
    datumRojstva = "1985-04-16";
  }

  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers:{
      "Authorization": getAuthorization()
    },
    success: function(data){
      var ehrId = data.ehrId;
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        additionalInfo: {"ehrId": ehrId}
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: "POST",
        headers:{
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party){
          if(party.action == 'CREATE'){
            //console.log(ehrId);
            console.log(partyData);
            if($("#opcije option").size() < 3){
              $("#opozorilo").css('display','none');  //Skrijemo začetno opozorilo o generiranju podatkov
              $("#opcije").append("<option value="+ehrId+">"+partyData.firstNames+" "+partyData.lastNames+" ("+partyData.dateOfBirth+")"+"</option>"); //Dodamo opcijo na select
              $("#opcije").css('visibility','visible'); //Nastavimo vidljiv select, saj je na začetku skrit
              $("#narisiStranke").css("display","block");
            }
            
            switch(stPacienta){
              case 1:{
                narediMeritevPacienta(ehrId,"2013-07-22","150.2","90.1","37.5","158","98","95");
                narediMeritevPacienta(ehrId,"2014-01-03","150.6","91.0","36.9","160","99","96");
                narediMeritevPacienta(ehrId,"2014-10-01","151.0","90.8","36.7","161","100","97");
                narediMeritevPacienta(ehrId,"2015-03-24","151.5","92.0","37.1","164","102","96");
                narediMeritevPacienta(ehrId,"2016-05-05","152.1","93.2","36.6","166","103","97");
                break;
              }
              case 2:{
                narediMeritevPacienta(ehrId,"2008-04-06","155.6","52.3","36.5","127","83","98");
                narediMeritevPacienta(ehrId,"2008-09-20","156.1","52.6","36.3","125","84","98");
                narediMeritevPacienta(ehrId,"2009-03-15","156.4","52.5","36.9","130","87","97");
                narediMeritevPacienta(ehrId,"2009-10-05","156.9","53.0","35.9","128","85","98");
                narediMeritevPacienta(ehrId,"2009-12-18","157.0","53.0","36.1","129","86","99");
                narediMeritevPacienta(ehrId,"2010-02-10","157.3","53.2","36.7","128","86","98");
                narediMeritevPacienta(ehrId,"2010-08-24","157.5","53.3","35.8","131","87","98");
                break;
              }
              case 3:{
                narediMeritevPacienta(ehrId,"2001-02-06","171.8","64.8","37.0","138","89","96");
                narediMeritevPacienta(ehrId,"2001-05-22","172.0","65.0","36.4","139","89","95");
                narediMeritevPacienta(ehrId,"2001-11-01","172.1","65.3","35.9","138","88","97");
                narediMeritevPacienta(ehrId,"2002-03-08","172.1","64.9","36.2","141","91","97");
                narediMeritevPacienta(ehrId,"2002-06-01","172.3","65.0","36.8","143","92","95");
                narediMeritevPacienta(ehrId,"2002-12-12","172.4","64.8","36.1","145","93","95");
                break;
              }
              
              return ehrId;
            }
          }
        },
        error: function(err){
          console.log(JSON.parse(err.responseText).userMessage);
        }
      })
    }
  })
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function sortirajJSONPoDatumu(a,b){//Funkcija za sortiranje rezultata poizvedbe po pacientovem krvnem pritisku
  return new Date(a.time).getTime() - new Date(b.time).getTime();
}

function preberiMeritveZaEHR(ehrId){//Funkcija prebere meritve določenega pacienta in sproži risanje grafa
  $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers:{
      "Authorization": getAuthorization()
    },
    success: function(data){
      console.log(data);
      $.ajax({
        url: baseUrl + "/view/" + ehrId + "/blood_pressure",
        type: 'GET',
        headers:{
          "Authorization": getAuthorization()
        },
        success: function(res){
          res.sort(sortirajJSONPoDatumu);
          console.log(res);
          ovrednotiStanjePacienta(res,data);
          vizualizirajMeritev(res);
          document.getElementById("vrstaTlaka").addEventListener("change",function(){
            vizualizirajMeritev(res);
          });
          
        }
      });
    },
    error: function(err){
      
    }
  });
}

function ovrednotiStanjePacienta(res,data){
  $("#pacientS").css("visibility","visible");
  $("#stanjePacienta").html("");
  $("#stanjePacienta").append("Stanje pacienta " + data.party.firstNames + " " + 
                      data.party.lastNames + ": <br/>");
                      
  $("#pacientN").css("visibility","visible");
  $("#nasvet").html("");
  
  if(res[res.length-1].systolic >= 140){
    $("#stanjePacienta").append("Pacient ima visok sistoličen tlak(" + res[res.length-1].systolic +
                      "), ki lahko povzroči zdravstvene težave<br/>");
  }
  else{
    $("#stanjePacienta").append("Pacient ima nizek sistoličen tlak(" + res[res.length-1].systolic +
                      "), ki ne prinaša zdravstvenih težav<br/>");
  }
  if(res[res.length-1].diastolic >= 90){
    $("#stanjePacienta").append("Pacient ima visok diastoličen tlak(" + res[res.length-1].diastolic +
                      "), ki lahko povzroči zdravstvene težave<br/>");
  }
  else{
    $("#stanjePacienta").append("Pacient ima nizek diastoličen tlak(" + res[res.length-1].diastolic +
                      "), ki ne prinaša zdravstvenih težav<br/>");
  }
  
  if(res[res.length-1].systolic < 140 && res[res.length-1].diastolic < 90){
    $.ajax({
    url: "https://icanhazdadjoke.com/",
    type: "GET",
    headers:{
      Accept: "text/plain"
      },
    success: function(res){
      console.log(res);
        $("#nasvet").append("Pacient ima vredu sistoličen in diastoličen krvni tlak.\
        Ker je smeh pol zdravja pa dodajamo še šalo, v upanju da bo pacient še dalje zdrav: <br/>" + res);
      }
    });
  }
  else{
    $.ajax({
      url:"https://faroo-faroo-web-search.p.rapidapi.com/api?q=health+tips",
      type: "GET",
      headers:{
        "X-RapidAPI-Host": "faroo-faroo-web-search.p.rapidapi.com",
        "X-RapidAPI-Key": "e8c532c79fmshfe55f8fb0dd986cp1b4a96jsn3ccf444d3ebe"
      },
      success:function(res){
        $("#nasvet").append("Pacient ni popolnoma zdrav, zato priporočamo bolj zdrav življenski stil\
        , tu je nekaj predlogov(iskanje s FAROO API-jem): <br/>");
        console.log(res);
        for(var i = 0; i < res.results.length; i++){
          $("#nasvet").append("<a href='"+res.results[i].url+"'>"+res.results[i].kwic+"</a><br/>");
        }
      }
    });
  }
  
  
  
}

function generiranjePodatkov(){//Funkcija 3-krat kliče generiranje podatkov
    if($("#opcije option").size() < 3){
      for(var i = 1; i <= 3; i++){
        generirajPodatke(i);
      }
    }
  }

function kreirajLegendo(){
  var svg = d3.select("#legenda")
      .append("svg")
      .attr("width",300)
      .attr("height",550);
  svg.append("circle").attr("cx",50).attr("cy",200).attr("r", 6).style("fill", "#00e600");
  svg.append("circle").attr("cx",50).attr("cy",225).attr("r", 6).style("fill", "#668cff");
  svg.append("circle").attr("cx",50).attr("cy",250).attr("r", 6).style("fill", "#ff9966");
  svg.append("circle").attr("cx",50).attr("cy",275).attr("r", 6).style("fill", "#996633");
  svg.append("circle").attr("cx",50).attr("cy",300).attr("r", 6).style("fill", "#cc3300");
  svg.append("text").attr("x", 70).attr("y", 200).text("optimalni tlak").style("font-size", "15px").attr("alignment-baseline","middle");
  svg.append("text").attr("x", 70).attr("y", 225).text("normalni tlak").style("font-size", "15px").attr("alignment-baseline","middle");
  svg.append("text").attr("x", 70).attr("y", 250).text("visoki normalni tlak").style("font-size", "15px").attr("alignment-baseline","middle");
  svg.append("text").attr("x", 70).attr("y", 275).text("blaga arterijska hipertenzija").style("font-size", "15px").attr("alignment-baseline","middle");
  svg.append("text").attr("x", 70).attr("y", 300).text("zmerna arterijska hipertenzija").style("font-size", "15px").attr("alignment-baseline","middle");
}

function vseMeritveZaLeto(rezultatMeritve,meritev){
  console.log(rezultatMeritve);
  console.log(meritev.getFullYear());
  $("#masterDetail").html("");
  $("#masterDetailP").css("visibility","visible");
  var html = "<table class='table'><thead>\
  <tr><th scope='col'>#</th>\
  <th scope='col'>Datum</th>\
  <th scope='col'>Sistolični tlak</th>\
  <th scope='col'>Diastolični tlak</th>\
  </tr>\
  </thead>\
  <tbody>";
  var count = 1;
  for(var i = 0; i < rezultatMeritve.length; i++){
    var tmpDat = new Date(rezultatMeritve[i].time);
    var tmp = tmpDat.getDate() + "-" + tmpDat.getMonth() + "-" + tmpDat.getFullYear();
    if(tmpDat.getFullYear() == meritev.getFullYear()){
      html += "<tr>\
      <th scope='row'>"+ count +"</th>\
      <td>" + tmp + "</td>\
      <td>" + rezultatMeritve[i].systolic + "</td>\
      <td>" + rezultatMeritve[i].diastolic + "</td>\
      </tr>";
      count++;
    }
  }
  html += "</tbody></table>";
  $("#masterDetail").append(html);
}

function vizualizirajMeritev(rezultatMeritve){
  console.log("test");
  $("svg").remove(); //odstrani morebitne obstoječe grafe
  $("#legenda").html(""); //izbriše morebitno legendo prejšnjega grafa
  $(".tooltip").remove();
  
  var kopijaRezultata = rezultatMeritve;
  
  var margin = {top:30, right:30, bottom:30, left:60},
      width = 750 - margin.left - margin.right,
      height = 550 - margin.top - margin.bottom;
  var svg = d3.select("#prikazGrafa")
      .append("svg")
        .attr("width",width + margin.left + margin.right)
        .attr("height",height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.right + ")");
  var format = d3.timeParse("%Y-%m-%d");
  
  if($("#vrstaTlaka").val() == 'systolic'){
    var max = 0;
    var min = rezultatMeritve[0].systolic;
    
    rezultatMeritve.forEach(function(d){
      var dat = new Date(d.time);
      d.time = dat.getFullYear() + "-" + dat.getMonth() + "-" + dat.getDate();
      d.time = format(d.time);
      console.log(d.time);
      if(d.systolic > max) max = d.systolic;
      if(d.systolic < min) min = d.systolic;
    });
    console.log(rezultatMeritve);
    var x = d3.scaleTime()
            .domain([rezultatMeritve[0].time,rezultatMeritve[rezultatMeritve.length-1].time])
            .range([0,width]);
    var y = d3.scaleLinear()
            .domain([min-20,max+20])
            .range([height,0]);
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));
    svg.append("g")
        .call(d3.axisLeft(y));
  
    var Tooltip = d3.select("#prikazGrafa")
        .append("div")
        .style("opacity", 0)
        .attr("class", "tooltip")
        .style("background-color", "white")
        .style("border", "solid")
        .style("border-width", "2px")
        .style("border-radius", "5px")
        .style("padding", "5px");
    var mouseover = function(d) {
      Tooltip
        .style("opacity", 1)
    }
    var mousemove = function(d) {
      var dat = new Date(d.time);
      var tmp = dat.getDate() + "-" + dat.getMonth() + "-" + dat.getFullYear();
      Tooltip
        .html("Dan meritve: " + tmp + "<br/>" + "Sistolični tlak: " + d.systolic)
        .style("left", (d3.mouse(this)[0]+70) + "px")
        .style("top", (d3.mouse(this)[1]+70) + "px")
    }
    var mouseleave = function(d) {
      Tooltip
        .style("opacity", 0)
    }
    var click = function(d){
      var dat = new Date(d.time);
      vseMeritveZaLeto(kopijaRezultata,dat);
    }
      
    svg.append("path")//Risanje meje za optimalni krvni tlak
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#00e600")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d){ return x(d.time)})
          .y(function(d){ return y(119)})
          );
    svg.append("path")//Risanje meje za normalni krvni tlak
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#668cff")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d){ return x(d.time)})
          .y(function(d){ return y(129)})
          );
    svg.append("path")//Risanje meje za visoko normalni krvni tlak
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#ff9966")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d){ return x(d.time)})
          .y(function(d){ return y(139)})
          );
    svg.append("path")//Risanje meje za blago arterijsko hipertenzijo
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#996633")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d){ return x(d.time)})
          .y(function(d){ return y(159)})
          );      
    svg.append("path")//Risanje meje za zmerno arterijsko hipertenzijo
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#cc3300")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d){ return x(d.time)})
          .y(function(d){ return y(179)})
          );     
          
          
    svg.append("path")
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#ff00ff")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d) { return x(d.time) })
          .y(function(d) { return y(d.systolic) })
          );
    
    svg
        .append("g")
        .selectAll("dot")
        .data(rezultatMeritve)
        .enter()
        .append("circle")
          .attr("cx", function(d) { return x(d.time) } )
          .attr("cy", function(d) { return y(d.systolic) } )
          .attr("r", 5)
          .attr("fill", "#ff00ff")
          .on("click", click)
          .on("mouseover", mouseover)
          .on("mousemove", mousemove)
          .on("mouseleave", mouseleave);
  }
  else{
    var max = 0;
    var min = rezultatMeritve[0].diastolic;
    
    rezultatMeritve.forEach(function(d){
      var dat = new Date(d.time);
      d.time = dat.getFullYear() + "-" + dat.getMonth() + "-" + dat.getDate();
      d.time = format(d.time);
      console.log(d.time);
      if(d.diastolic > max) max = d.diastolic;
      if(d.diastolic < min) min = d.diastolic;
    });
    console.log(rezultatMeritve);
    var x = d3.scaleTime()
            .domain([rezultatMeritve[0].time,rezultatMeritve[rezultatMeritve.length-1].time])
            .range([0,width]);
    var y = d3.scaleLinear()
            .domain([min-5,max+5])
            .range([height,0]);
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));
    svg.append("g")
        .call(d3.axisLeft(y));
  
    var Tooltip = d3.select("#prikazGrafa")
        .append("div")
        .style("opacity", 0)
        .attr("class", "tooltip")
        .style("background-color", "white")
        .style("border", "solid")
        .style("border-width", "2px")
        .style("border-radius", "5px")
        .style("padding", "5px");
    var mouseover = function(d) {
      Tooltip
        .style("opacity", 1)
    }
    var mousemove = function(d) {
      var dat = new Date(d.time);
      var tmp = dat.getDate() + "-" + dat.getMonth() + "-" + dat.getFullYear();
      Tooltip
        .html("Dan meritve: " + tmp + "<br/>" + "Diastolični tlak: " + d.diastolic)
        .style("left", (d3.mouse(this)[0]+70) + "px")
        .style("top", (d3.mouse(this)[1]+70) + "px")
    }
    var mouseleave = function(d) {
      Tooltip
        .style("opacity", 0)
    }
    var click = function(d){
      var dat = new Date(d.time);
      vseMeritveZaLeto(kopijaRezultata,dat);
    }
      
    svg.append("path")//Risanje meje za optimalni krvni tlak
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#00e600")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d){ return x(d.time)})
          .y(function(d){ return y(79)})
          );
    svg.append("path")//Risanje meje za normalni krvni tlak
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#668cff")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d){ return x(d.time)})
          .y(function(d){ return y(84)})
          );
    svg.append("path")//Risanje meje za visoko normalni krvni tlak
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#ff9966")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d){ return x(d.time)})
          .y(function(d){ return y(89)})
          );
    svg.append("path")//Risanje meje za blago arterijsko hipertenzijo
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#996633")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d){ return x(d.time)})
          .y(function(d){ return y(99)})
          );      
    svg.append("path")//Risanje meje za zmerno arterijsko hipertenzijo
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#cc3300")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d){ return x(d.time)})
          .y(function(d){ return y(109)})
          );     
          
          
    svg.append("path")
        .datum(rezultatMeritve)
        .attr("fill", "none")
        .attr("stroke", "#ff00ff")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
          .x(function(d) { return x(d.time) })
          .y(function(d) { return y(d.diastolic) })
          );
    
    svg
        .append("g")
        .selectAll("dot")
        .data(rezultatMeritve)
        .enter()
        .append("circle")
          .attr("cx", function(d) { return x(d.time) } )
          .attr("cy", function(d) { return y(d.diastolic) } )
          .attr("r", 5)
          .attr("fill", "#ff00ff")
          .on("click", click)
          .on("mouseover", mouseover)
          .on("mousemove", mousemove)
          .on("mouseleave", mouseleave);
  }
  
  kreirajLegendo();
}

function narediMeritevPacienta(ehrId, datumInUra, telesnaVisina, telesnaTeza, telesnaTemperatura,
    sistolicniKrvniTlak,diastolicniKrvniTlak,nasicenostKrviSKisikom){
      var dat = {
        "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
      };
      var parameters = {
        ehrId: ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT'
      };
      $.ajax({
        url: baseUrl + "/composition?" + $.param(parameters),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(dat),
        headers: {
          "Authorization": getAuthorization()
        },
        success: function(res){
          
        },
        error: function(err){
          
        }
      });
    }

window.addEventListener("load", function(){
  
  function pridobiBolnice(callback){
      var xobj = new XMLHttpRequest();
		  xobj.overrideMimeType("application/json");
			xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
			xobj.onreadystatechange = function()
			{
				 if (xobj.readyState == 4 && xobj.status == "200") {
        			var json = JSON.parse(xobj.responseText);
        			console.log(json);
        			callback(json);
				 }
			}
			xobj.send(null);
  }
  
  function izpisB(json){
    var podatki = json.features;
    for(var i = 0; i < podatki.length; i++){
      console.log(podatki[i]);
      if(podatki[i].geometry.type == "Polygon"){
        if(podatki[i].geometry.coordinates.length == 2){
          var latlngs_1=[];
          for(var j = 0; j < podatki[i].geometry.coordinates[0].length; j++){ //Obrnemo koordinate, saj Leaflet pričakuje koordinate v obrnjenm vrstem redu
            var tmp = [podatki[i].geometry.coordinates[0][j][1],podatki[i].geometry.coordinates[0][j][0]];
            latlngs_1.push(tmp);
          }
          var latlngs_2=[];
          for(var j = 0; j < podatki[i].geometry.coordinates[1].length; j++){ //Obrnemo koordinate, saj Leaflet pričakuje koordinate v obrnjenm vrstem redu
            var tmp = [podatki[i].geometry.coordinates[1][j][1],podatki[i].geometry.coordinates[1][j][0]];
            latlngs_2.push(tmp);
          }
          var polygon = L.polygon([latlngs_1,latlngs_2],{color: 'blue'});
          if(podatki[i].properties.name != undefined && podatki[i].properties["addr:street"] != undefined && podatki[i].properties["addr:housenumber"] != undefined)
            polygon.bindPopup(podatki[i].properties.name + "<br/>" + podatki[i].properties["addr:street"] + " " + podatki[i].properties["addr:housenumber"]).addTo(map);
          else
            polygon.bindPopup("Primanjkujejo podatki!").addTo(map);
          map.addLayer(polygon);
          layers.push(polygon);
          bolnice.push(podatki[i]);
        }
        else{
          var latlngs = [];
          for(var j = 0; j < podatki[i].geometry.coordinates[0].length; j++){ //Obrnemo koordinate, saj Leaflet pričakuje koordinate v obrnjenm vrstem redu
            var tmp = [podatki[i].geometry.coordinates[0][j][1],podatki[i].geometry.coordinates[0][j][0]];
            latlngs.push(tmp);
          }
          var polygon = L.polygon(latlngs,{color: 'blue'});
          if(podatki[i].properties.name != undefined && podatki[i].properties["addr:street"] != undefined && podatki[i].properties["addr:housenumber"] != undefined)
            polygon.bindPopup(podatki[i].properties.name + "<br/>" + podatki[i].properties["addr:street"] + " " + podatki[i].properties["addr:housenumber"]).addTo(map);
          else
            polygon.bindPopup("Primanjkujejo podatki!").addTo(map);
          map.addLayer(polygon);
          layers.push(polygon);
          bolnice.push(podatki[i]);
        }
      }
    }
  }
  
  function mapClicked(e){
    var latlng = e.latlng;
    console.log(latlng);
    for(var i = 0; i < layers.length; i++){
      var tmp = layers[i].getBounds().getCenter();
      if(distance(latlng["lat"],latlng["lng"],tmp["lat"],tmp["lng"],"K") < 5)
        layers[i].setStyle({color:'green'});
      else
        layers[i].setStyle({color:'blue'});
    }
  }
  
  var mapOptions = {
    center: [46.05004, 14.46931],
    zoom: 12
  };
  
  if(document.getElementById("mapa_id") != undefined){
    map = new L.map('mapa_id',mapOptions);
    var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    map.addLayer(layer);
    pridobiBolnice(izpisB);
    
    map.on("click",mapClicked);
  }
  
  //generiranjePodatkov();
  
  document.getElementById("generiranjePodatkov").addEventListener("click",function(){
    generiranjePodatkov();
  });
  
  document.getElementById("narisiStranke").addEventListener("click",function(){
    preberiMeritveZaEHR($("#opcije").val());
  });
  
  document.getElementById("preberiObstojeci").addEventListener("click",function() {
    preberiMeritveZaEHR($("#preberiEHRid").val());
  });
  
  
});
